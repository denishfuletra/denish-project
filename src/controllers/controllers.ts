import { crud, data } from "../repositories/repositories";

const method = new crud();

export class Admin {
  addData = async (data: data): Promise<any> => {
    try {
      const response = await method.addData(data);
      if (response.message === "data is inserted successfully") {
        console.log(response);
        return {
          status: 200,
          response,
        };
      }
    } catch (error) {
      return { status: 500, error: error };
    }
  };

  deleteData = async (id: Number): Promise<any> => {
    try {
      const response = await method.deleteData(id);
      if (response.message === "data is deleted successfully") {
        return {
          status: 200,
          response,
        };
      }
    } catch (error) {
      return { status: 500, error: error };
    }
  };

  update_task_status = async (id: Number, status: Number): Promise<any> => {
    try {
      const response = await method.updateTaskStatus(id, status);
      if (response.message === "task status is updated successfully") {
        return {
          status: 200,
          response,
        };
      }
    } catch (error) {
      return { status: 500, error: error };
    }
  };

  update_task = async (data: data): Promise<any> => {
    try {

      const response_get_task_by_id = await method.get_task_by_id(data.id);
      // console.log(response);
      const get_data_from_server = response_get_task_by_id.result[0]; // get data from server using user_id
      console.log('get data from server', get_data_from_server); // print the data whichi is received from server

      const will_update_data: data = {
        id: data.id,
        task_name: data.task_name ? data.task_name : get_data_from_server[0].task_name,
        completion: data.completion === undefined ? get_data_from_server[0].completion : data.completion
      };
      //console.log(get_data_from_server[0].completion);
      //console.log('will update data', will_update_data);

      const response_update_data = await method.update_task(will_update_data);

      return {
        status: 200,
        response_update_data,
      };
    } catch (error) {
      return { status: 500, error: error };
    }
  };
}
