import mysql from 'mysql';

const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'todo_tasks'
})


pool.getConnection((err, connection) => {
    if (err) {
        throw err;
    }
    console.log('connected to mysql database');
    connection.release();
});


export default pool;