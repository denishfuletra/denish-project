import db from "../config/database";
const util = require("util");

export interface data {
  id: Number;
  task_name: String;
  completion: Number;
}

const query = util.promisify(db.query).bind(db);
export class crud {
  // addData = (data: object): Promise<any> => {
  //     return new Promise((resolve, reject) => {
  //         db.query('INSERT INTO TODO SET ?', data, (err, result) => {
  //             if (err) {
  //                 reject(err);
  //             } else {
  //                 resolve({
  //                     message: "data is inserted successfully",
  //                     result: result
  //                 });
  //             }
  //         })
  //     });
  // }
  // addData = async (data: object) => {
  //     try {
  //         const result = await query('INSERT INTO TODO SET ?', data);
  //         console.log(result); // log the result object
  //         return {
  //             message: 'data is inserted successfully',
  //             result: result,
  //         };
  //     } catch (error) {
  //         console.error(error); // log the error object
  //         throw error;
  //     }
  // };

  addData = async (data: data): Promise<any> => {
    try {
      const { task_name, completion } = data;
      const result: any = await query("CALL insert_task(?,?)", [
        task_name,
        completion,
      ]);
      //console.log(result); // log the result object
      return {
        message: "data is inserted successfully",
        result: result,
      };
    } catch (error) {
      // console.error(error); // log the error object
      throw error;
    }
  };

  deleteData = async (id: Number): Promise<any> => {
    try {
      const result: any = await query("CALL delete_task(?)", id);
      //console.log(result);
      return {
        message: "data is deleted successfully",
        result: result,
      };
    } catch (err) {
      console.log(err); // log the error object
      throw err;
    }
  };

  updateTaskStatus = async (id: Number, status: Number): Promise<any> => {
    try {
      const result: any = await query("CALL update_task_status(?,?)", [
        id,
        status,
      ]);
      return {
        message: "task status is updated successfully",
        result,
      };
    } catch (err) {
      throw err;
    }
  };

  get_task_by_id = async (id: Number): Promise<any> => {
    try {
      const result: any = await query("CALL get_task_by_id(?)", id);
      return {
        message: "task status is updated successfully",
        result,
      };
    } catch (err) {
      throw err;
    }
  };
  update_task = async (data: data): Promise<any> => {
    try {
      const { id, task_name, completion } = data;
      // console.log(id,task_name,completion);
      const result: any = await query("CALL update_task(?,?,?)", [id, task_name, completion]);
      return {
        message: "task status is updated successfully",
        result,
      };
    } catch (err) {
      throw err;
    }
  };
}
