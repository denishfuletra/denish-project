import { Router, Response, Request } from "express";
import express from "express";
import { crud, data } from "../../repositories/repositories";
import { Admin } from "../../controllers/controllers";

const router: Router = express.Router();

const route = () => {
  const admin = new Admin();
  router.get("/", (req: Request, res: Response) => {
    res.status(200).send("Hello Deny");
  });

  // router.post('/add', async (req: Request, res: Response) => {
  //     try {
  //         const response = await method.addData(req.body);
  //         //console.log(response);
  //         return res.status(200).send(response);
  //     } catch (error) {
  //         //console.error(error);
  //         return res.status(500).send("internal server error");
  //     }
  // });
  router.post("/add", async (req: Request, res: Response) => {
    const result = await admin.addData(req.body);
    console.log(result);
    return res.status(result.status).send(result);
  });

  router.delete("/delete/:id", async (req: Request, res: Response) => {
    const result = await admin.deleteData(+req.params.id);
    return res.status(result.status).send(result);
  });

  router.patch("/update/:id", async (req: Request, res: Response) => {
    const result = await admin.update_task_status(
      +req.params.id,
      +req.body.status
    );
    return res.status(result.status).send(result);
  });

  router.patch("/taskupdate", async (req: Request, res: Response) => {
    //console.log(req.body);
    const result = await admin.update_task(req.body);
    return res.status(result.status).send(result);
  });
};

route();

export default router;
