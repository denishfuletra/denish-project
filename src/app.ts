import express from 'express'
import { Request, Response, Application, Router } from 'express';
const cors = require('cors');
import route from './routes/private/routes'

export const app: Application = express();

app.use(express.json());

app.use(cors());

app.use('/api', route);
